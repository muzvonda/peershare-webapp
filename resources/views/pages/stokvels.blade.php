@extends('../layout/' . $layout)

@section('subhead')
    <title>Stokvels - PeerShare</title>
@endsection

@section('subcontent')
    <h2 class="intro-y text-lg font-medium mt-10">My Stokvels</h2>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
            <button class="button text-white bg-theme-1 shadow-md mr-2">
                <a href="/create-stokvel">Create A Stokvel</a>
            </button>
            <button class="button text-white bg-theme-1 shadow-md mr-2">
                <a href="/community-stokvels">Join Stokvels</a>
            </button>
            
          
            {{-- <div class="hidden md:block mx-auto text-gray-600">Showing 1 to 10 of 150 entries</div>
            <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                <div class="w-56 relative text-gray-700 dark:text-gray-300">
                    <input type="text" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                    <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
                </div>
            </div> --}}
        </div>
        <!-- BEGIN: Stokvels Layout -->

        @foreach ($stokvels as $stokvel)
            <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4">
                <div class="box">
                    <div class="flex items-start px-5 pt-5">
                        <div class="w-full flex flex-col lg:flex-row items-center">
                            <div class="w-16 h-16 image-fit">
                                <i style="color:#1C3FAA" data-feather="target" class="w-16 h-16"></i>
                                {{-- <img alt="Midone Tailwind HTML Admin Template" class="rounded-md" src="{{ asset('dist/images/' . $faker['photos'][0]) }}"> --}}
                            </div>
                            <div class="lg:ml-4 text-center lg:text-left mt-3 lg:mt-0">
                                <a style="color:black" href="" class="font-medium">{{ $stokvel->stokvel_name }}</a>
                                <div class="text-gray-600 text-xs">Population: {{ $stokvel->population }}/{{ $stokvel->population_limit }}</div>
                            </div>
                        </div>
                       
                    </div>
                    <div class="text-center lg:text-left p-5">
                        {{-- <div>{{ $faker['news'][0]['short_content'] }}</div> --}}
                        {{-- <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-5">
                            <i data-feather="mail" class="w-3 h-3 mr-2"></i>Registered:  
                        </div> --}}
                        <div style="font-size: 13px;color:#ffffff; font-weight: bold;background-color:#1C3FAA;border-radius:3px;padding:5px" class="flex items-center justify-center lg:justify-start text-black mt-1">
                            <i data-feather="dollar-sign" class="w-3 h-3 mr-2"></i><span style="color:#1C3FAA; font-weight: bold;background-color:rgb(248, 248, 248);border-radius:3px;padding:5px; margin:2px"> Project Types </span> 
                            
                            @foreach( json_decode($stokvel->project_types) as $projectType )
                             {{ $projectType }},
                            @endforeach
                        </div>
                        <div style="font-size: 13px" class="flex items-center justify-center lg:justify-start  text-black mt-1">
                            <i data-feather="check" class="w-3 h-3 mr-2"></i><span style="color:#1C3FAA; font-weight: bold;border-radius:3px;padding:5px; margin:2px"> Verified Stokvel </span> {{ $stokvel->name }}
                        </div>
                    </div>
                    <div class="text-center lg:text-right p-5 border-t border-gray-200 dark:border-dark-5">
                        
                        {{-- <button class="button button--sm text-gray-700 border border-gray-300 dark:border-dark-5 dark:text-gray-300">View</button> --}}
                    </div>
                </div>
            </div>
        @endforeach
        <!-- END: Stokvels Layout -->
        <!-- BEGIN: Pagination -->
        {{-- <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-no-wrap items-center">
            <ul class="pagination">
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevrons-left"></i>
                    </a>
                </li>
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevron-left"></i>
                    </a>
                </li>
                <li>
                    <a class="pagination__link" href="">...</a>
                </li>
                <li>
                    <a class="pagination__link" href="">1</a>
                </li>
                <li>
                    <a class="pagination__link pagination__link--active" href="">2</a>
                </li>
                <li>
                    <a class="pagination__link" href="">3</a>
                </li>
                <li>
                    <a class="pagination__link" href="">...</a>
                </li>
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevron-right"></i>
                    </a>
                </li>
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevrons-right"></i>
                    </a>
                </li>
            </ul>
            <select class="w-20 input box mt-3 sm:mt-0">
                <option>10</option>
                <option>25</option>
                <option>35</option>
                <option>50</option>
            </select>
        </div> --}}
        <!-- END: Pagination -->
    </div>
@endsection