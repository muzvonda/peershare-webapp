@extends('../layout/' . $layout)

@section('subhead')
    <title>PeerShare - Create Stokvel</title>
@endsection

@section('subcontent')
<style>
    label{
        color: #1C3FAA; font-size: 13px; font-weight: bold; padding: 5px; background-color: rgb(243, 243, 243); border-radius: 3px
    }    
    </style>
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">New Stokvel</h2>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Form Layout -->
            <form method="post" action="{{ route('savestokvel') }}">
                {{ csrf_field() }}
                <div class="intro-y box p-5">
                    <div>
                        <label>Stokvel Name</label>
                        <input type="text" name="stokvel_name" class="input w-full border mt-2" placeholder="Give your project a name">
                    </div>
                    <div class="mt-3">
                        <label>Stokvel Capacity Limit</label>
                        <div class="relative mt-2">
                            <input type="number" name="stokvel_capacity" class="input pr-16 w-full border col-span-4" placeholder="Max number of people allowed">
                            <div class="absolute top-0 right-0 rounded-r w-16 h-full flex items-center justify-center bg-gray-100 dark:bg-dark-1 dark:border-dark-4 border text-gray-600">people</div>
                        </div>
                    </div>
                    <div class="mt-3">
                        <label>Projects Types</label>
                        <div class="mt-2">
                            <select multiple name="project_types[]" data-placeholder="Type(s) of allowed project(s)" class="tail-select w-full">
                                <option value="Groceries">Groceries</option>
                                <option value="Savings">Savings</option>
                                <option value="School Fees">School Fees</option>
                                <option value="Housing">Housing</option>
                                <option value="Travel">Travel</option>
                            </select>
                        </div>
                    </div>


                    </div>

                    <div class="text-right mt-5">
                        {{-- <button type="button" class="button w-24 border dark:border-dark-5 text-gray-700 dark:text-gray-300 mr-1">Cancel</button> --}}
                        <button type="submit" class="button w-24 bg-theme-1 text-white">Create</button>
                    </div>
                </div>
            </form>
            <!-- END: Form Layout -->
        </div>
    </div>    
@endsection