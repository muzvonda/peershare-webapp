
@extends('../layout/' . $layout)

@section('subhead')
    <title>PeerShare - Registration Fee</title>
@endsection

@section('subcontent')
    <div class="intro-y box p-5 bg-theme-1 text-white mt-5">
        <div class="flex items-center">
            <div class="font-medium text-lg">Registration Fee Payment</div>
            <div class="text-xs bg-white dark:bg-theme-1 dark:text-white text-gray-800 px-1 rounded-md ml-auto">Alert</div>
        </div>
        <div class="mt-4">To start using PeerShare you are required to pay a Registration Fee of R25</div>
        
        <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
            <input type="hidden" name="email" value="{{ $userEmail }}"> 
                <input type="hidden" name="amount" value="2500"> 
                <input type="hidden" name="currency" value="ZAR">
                <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> 
        
                {{ csrf_field() }} 
    
                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

            <div class="font-medium flex mt-5">
                <button type="submit" class="button button--sm border border-white text-white dark:border-dark-5 dark:text-gray-300">Pay Now</button>
            </div>
        </form>
    </div>

    
@endsection