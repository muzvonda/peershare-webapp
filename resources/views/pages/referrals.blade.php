@extends('../layout/' . $layout)

@section('subhead')
    <title>PeerShare - My Referrals</title>
@endsection

@section('subcontent')
<script>
    function copyToClipboard(ref) {
        document.getElementById(id).select();
        document.execCommand('copy');
    }
</script>
    <h2 class="intro-y text-lg font-medium mt-10">My PeerShare Referrals</h2>
    <div class="grid grid-cols-12 gap-6 mt-5">
        
        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">

            <a class="button text-white bg-theme-1 shadow-md mr-2" >Referral code is your number: {{$referralLink}}</a>
            <a class="button text-white bg-theme-1 shadow-md mr-2" >Referral Wallet: R {{$referralBalance}}</a>
            <table class="table table-report -mt-2">
                <thead>
                    <tr>
                        <th class="whitespace-no-wrap">Referral Name</th>
                        <th class="whitespace-no-wrap">Registration Date</th>
                        <th class="whitespace-no-wrap">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($userReferrals as $referral)
                        <tr class="intro-x">
                            <td class="w-40">
                                <div class="flex">
                                    <div class="w-20 h-10 image-fit zoom-in">
                                       {{$referral->name}}
                                    </div>
                                </div>
                            </td>
                            <td class="w-40">
                                <div class="flex">
                                    <div class="w-40 h-10 image-fit zoom-in">
                                        {{$referral->created_at}}
                                    </div>
                                </div>
                            </td>
                            <td class="w-40">
                                <div class="flex">
                                    <div text-center class="w-40 h-10 image-fit zoom-in">
                                        @if( $referral->reg_fee === 1)
                                        <span class="button text-white bg-theme-1 shadow-md mr-2">Active</span>
                                        @else
                                        <span style="background-color: red; color: white" class="button text-white shadow-md mr-2">Not Active</span>
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- END: Data List -->
        <!-- BEGIN: Pagination -->
        {{-- <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-no-wrap items-center">
            <ul class="pagination">
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevrons-left"></i>
                    </a>
                </li>
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevron-left"></i>
                    </a>
                </li>
                <li>
                    <a class="pagination__link" href="">...</a>
                </li>
                <li>
                    <a class="pagination__link" href="">1</a>
                </li>
                <li>
                    <a class="pagination__link pagination__link--active" href="">2</a>
                </li>
                <li>
                    <a class="pagination__link" href="">3</a>
                </li>
                <li>
                    <a class="pagination__link" href="">...</a>
                </li>
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevron-right"></i>
                    </a>
                </li>
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevrons-right"></i>
                    </a>
                </li>
            </ul>
            <select class="w-20 input box mt-3 sm:mt-0">
                <option>10</option>
                <option>25</option>
                <option>35</option>
                <option>50</option>
            </select>
        </div> --}}
        <!-- END: Pagination -->
    </div>
    <!-- BEGIN: Delete Confirmation Modal -->
    {{-- <div class="modal" id="delete-confirmation-modal">
        <div class="modal__content">
            <div class="p-5 text-center">
                <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                <div class="text-3xl mt-5">Are you sure?</div>
                <div class="text-gray-600 mt-2">Do you really want to delete these records? This process cannot be undone.</div>
            </div>
            <div class="px-5 pb-8 text-center">
                <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                <button type="button" class="button w-24 bg-theme-6 text-white">Delete</button>
            </div>
        </div>
    </div> --}}
    <!-- END: Delete Confirmation Modal -->
@endsection