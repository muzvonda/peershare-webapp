@extends('../layout/' . $layout)

@section('subhead')
    <title>PeerShare - Create Project</title>
@endsection

@section('subcontent')
<style>
    label{
        color: #1C3FAA; font-size: 13px; font-weight: bold; padding: 5px; background-color: rgb(243, 243, 243); border-radius: 3px
    }    
    </style>
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">New Project</h2>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Form Layout -->
            <form method="post" action="">
                {{ csrf_field() }}
                <div class="intro-y box p-5">
                    <div>
                        <label>Project Name</label>
                        <input name="project_name" type="text" class="input w-full border mt-2" placeholder="Give your project a name" required>
                        
                    </div><br>
                    <div class="mt-3">
                        <label>Select Stockvel</label>
                        <div class="mt-2">
                            <select name="stokvel_id" data-placeholder="Select your project type" class="tail-select w-full" required>
                                @foreach ( $allStokvels as $stokvel )
                                <option value="{{ $stokvel->id }}"> {{ $stokvel->stokvel_name }} </option>
                                @endforeach
                            </select>
                            <p style="font-size:10px;color: rgb(0, 155, 0)">Make sure you have joined at least 1 stokvel before creating a project</p>
                    
                        </div><br>
                    </div>
                    
                    <div class="mt-3">
                        <label>Project Structure</label>
                        <div class="mt-2">
                            <select name="project_structure_id" data-placeholder="Select your project type" class="tail-select w-full" required>
                                @foreach ( $projectStructures as $projectStructure )
                                <option value="{{$projectStructure->id}}">{{$projectStructure->project_structure_name}}</option>
                                @endforeach
                            </select>
                            </div>
                            <div style="background-color: rgb(241, 241, 241); padding: 8px; border-radius: 5px">
                                <p style="font-size:10px;color: #1C3FAA; font-weight: bold"><span style="color: black; font-weight: bold">Peer to Peer:</span> A queue is created, where members take turns to receive gifts</p>
                                <p style="font-size:10px;color: #1C3FAA; font-weight: bold"><span style="color: black; font-weight: bold">Central Deposit:</span> A central wallet is created which is distributed after a selected period</p>
                            </div><br>
                    </div>
                    <div class="mt-3">
                        <label>Project Type</label>
                        <div class="mt-2">
                            <select name="project_type_id" data-placeholder="Select your project type" class="tail-select w-full" required>
                                @foreach ( $projectTypes as $projectType )
                                <option value="{{ $projectType->id }}">{{ $projectType->project_type_name }}</option>
                                @endforeach
                            </select>
                        </div><br>
                    </div>
                    
                    <div class="mt-3">
                        <label>Goal/Target</label>
                            <div class="relative mt-2">
                                <div class="absolute top-0 left-0 rounded-l w-12 h-full flex items-center justify-center bg-gray-100 dark:bg-dark-1 dark:border-dark-4 border text-gray-600">R</div>
                                <div class="pl-3">
                                    <input name="goal" type="number" class="input pl-12 w-full border col-span-4" placeholder="Set your Project Target" required>
                                </div>
                            </div><br>
                    </div>

                    <div class="mt-3">
                        <label>Plan</label>
                        <div class="mt-2">
                            <select name="plan_id" data-placeholder="Select your project type" class="tail-select w-full" required>
                                @foreach ( $plans as $plan )
                                <option value="{{ $plan->id }}">{{ $plan->plan_amount }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="text-right mt-5">
                        {{-- <button type="button" class="button w-24 border dark:border-dark-5 text-gray-700 dark:text-gray-300 mr-1">Cancel</button> --}}
                        <button type="submit" class="button w-24 bg-theme-1 text-white">Create</button>
                    </div>
                </div>
            <form>
            <!-- END: Form Layout -->
        </div>
    </div>    
@endsection