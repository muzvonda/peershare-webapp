
@extends('../layout/' . $layout)

@section('subhead')
    <title>PeerShare - Project</title>
@endsection

@section('subcontent')
    @foreach ($projectDetails as $project)
        <div class="intro-y box p-5 bg-theme-1 text-white mt-5">
            <div class="flex items-center">
                <div class="font-medium text-lg">{{ $project->project_name }}</div><br>
                
                <div class="text-xs bg-white dark:bg-theme-1 dark:text-white text-gray-800 px-1 rounded-md ml-auto">PeerShare</div>
            </div>

            <p>PeerShare algorithm automatically allocates you to the next payer or receiver when available so keep checking your project.</p>
            
                {{-- <div class="font-medium flex mt-5">
                    <button type="submit" class="button button--sm border border-white text-white dark:border-dark-5 dark:text-gray-300">Initiate Project</button>
                </div> --}}
            
        </div>

        {{-- Payer Transaction --}}
        @foreach ($paymentTransactions as $transaction)
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                </div>
            @endif

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There was a problem uploading your proof of payment
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            {{-- Show when user is allocated to pay --}}
            <div class="intro-y box p-5 bg-theme-1 text-white mt-5">
                <div class="flex items-center">
                    <div class="font-small text-md">
                        <p>You have been allocated to make payment</p><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Send</span> R{{$transaction->transaction_amount}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">To</span> {{$transaction->name}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Bank Name</span> {{$transaction->bank_name}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Bank Account</span> {{$transaction->acc_number}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Phone Number</span> {{$transaction->phone_number}}</span> <br><br>
                        
                        <form method="POST" action="{{ route('file.upload.post') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="project_id" value="{{$transaction->project}}"> 
                            <input type="hidden" name="transaction_id" value="{{$transaction->transaction_id}}"> 
                            <input type="hidden" name="payment" value="first"> 
                            <input type="hidden" name="transaction_amount" value="{{$transaction->transaction_amount}}"> 
                            <input type="hidden" name="payer_id" value="{{$transaction->payer_id}}">
                            <input type="hidden" name="receiver_id" value="{{$transaction->receiver_id}}"> 

                            <input style="background-color: white; color: black; border-radius: 5px" name="file" type="file" />
                            <p>Use your full name as reference</p>

                            <div class="font-medium flex mt-5">
                                <button type="submit" class="button button--sm border border-white text-white dark:border-dark-5 dark:text-gray-300">Upload Payment Proof</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach

        {{-- Payer's transactions waiting for receiver approval --}}
        @foreach ($approvalTransactions as $transaction)
            {{-- Show when user transaction is waiting for approval --}}
            <div class="intro-y box p-5 bg-theme-1 text-white mt-5">
                <div class="flex items-center">
                    <div class="font-small text-md">
                        
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Amount Sent</span> R{{$transaction->transaction_amount}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">To</span> {{$transaction->name}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Bank Name</span> {{$transaction->bank_name}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Bank Account</span> {{$transaction->acc_number}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Phone Number</span> {{$transaction->phone_number}}</span> <br><br>
                        <div class="font-medium flex mt-5">
                            <button class="button button--sm border border-white text-white dark:border-dark-5 dark:text-gray-300">Waiting for Approval</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        {{-- Receiver's transactions waiting to be approved --}}
        @foreach ($approverTransactions as $transaction)
            {{-- Show when user transaction is waiting for approval --}}
            <div class="intro-y box p-5 bg-theme-1 text-white mt-5">
                <div class="flex items-center">
                    <div class="font-small text-md">
                        
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Amount Received</span> R{{$transaction->transaction_amount}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">From</span> {{$transaction->name}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Phone Number</span> {{$transaction->phone_number}}</span> <br><br>
                        <p>Click approve if you have received R{{$transaction->transaction_amount}} from {{$transaction->name}}</p>
                        <p>If didn't receive the money, contact {{$transaction->name}} on {{$transaction->phone_number}}</p>
                        <form method="POST" action="{{ route('approve') }}">
                            {{ csrf_field() }}
                        <div class="font-medium flex mt-5">
                            <input type="hidden" name="transaction_id" value="{{$transaction->transaction_id}}"> 
                            <input type="hidden" name="project_id" value="{{$transaction->project}}"> 
                            <input type="hidden" name="transaction_amount" value="{{$transaction->transaction_amount}}"> 
                            <input type="hidden" name="payer_id" value="{{$transaction->payer_id}}"> 
                            <button class="button button--sm border border-white text-white dark:border-dark-5 dark:text-gray-300">Approve Payment</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach

            {{-- Receiver's transactions waiting to be paid --}}
            @foreach ($receiverTransactions as $transaction)
                {{-- Show when user transaction is waiting for approval --}}
                <div class="intro-y box p-5 bg-theme-1 text-white mt-5">
                    <div class="flex items-center">
                        <div class="font-small text-md">
                            <p>Waiting for payment</p><br>
                            
                            <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Amount To Receive</span> R{{$transaction->transaction_amount}}</span> <br><br>
                            <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">From</span> {{$transaction->name}}</span> <br><br>
                            <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Phone Number</span> {{$transaction->phone_number}}</span> <br><br>
                            <p>You have been allocated to receive R{{$transaction->transaction_amount}} from {{$transaction->name}}</p>
                            <p>To confirm payment, you may contact {{$transaction->name}} on {{$transaction->phone_number}}</p>
                            <button class="button button--sm border border-white text-white dark:border-dark-5 dark:text-gray-300"><a href="tel://{{$transaction->phone_number}}">Click to Call</a></button>
                        </div>
                    </div>
                </div>
            @endforeach

            {{-- Receiver's recommit transactions waiting to be paid --}}
            @foreach ($receiverCommitTransactions as $transaction)
            
                <div class="intro-y box p-5 bg-theme-1 text-white mt-5">
                    <div class="flex items-center">
                        <div class="font-small text-md">
                            <p>Waiting for payment</p><br>
                            
                            <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Amount To Receive</span> R{{$transaction->transaction_amount}}</span> <br><br>
                            <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">From</span> {{$transaction->name}}</span> <br><br>
                            <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Phone Number</span> {{$transaction->phone_number}}</span> <br><br>
                            <p>You have been allocated to receive R{{$transaction->transaction_amount}} from {{$transaction->name}}</p>
                            <p>To confirm payment, you may contact {{$transaction->name}} on {{$transaction->phone_number}}</p>
                            <button class="button button--sm border border-white text-white dark:border-dark-5 dark:text-gray-300"><a href="tel://{{$transaction->phone_number}}">Click to Call</a></button>
                        </div>
                    </div>
                </div>
            @endforeach

        {{-- Payer Recommit Transactions--}}
        @foreach ($recommitTransactions as $transaction)
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There was a problem uploading your proof of payment
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{-- Show when user is allocated to pay --}}
            <div class="intro-y box p-5 bg-theme-1 text-white mt-5">
                <div class="flex items-center">
                    <div class="font-small text-md">
                        <p>You next stage is withdrawal, recommit to proceed</p><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Send</span> R{{$transaction->transaction_amount}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">To</span> {{$transaction->name}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Bank Name</span> {{$transaction->bank_name}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Bank Account</span> {{$transaction->acc_number}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Phone Number</span> {{$transaction->phone_number}}</span> <br><br>
                    
                        <form method="POST" action="{{ route('file.upload.post') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="project_id" value="{{$transaction->project}}"> 
                            <input type="hidden" name="transaction_id" value="{{$transaction->transaction_id}}">
                            <input type="hidden" name="payment" value="second"> 
                            <input type="hidden" name="transaction_amount" value="{{$transaction->transaction_amount}}"> 
                            <input type="hidden" name="payer_id" value="{{$transaction->payer_id}}">
                            <input type="hidden" name="receiver_id" value="{{$transaction->receiver_id}}"> 

                            <input style="background-color: white; color: black; border-radius: 5px" name="file" type="file" />
                            <p>Use your full name as reference</p>

                            <div class="font-medium flex mt-5">
                                <button type="submit" class="button button--sm border border-white text-white dark:border-dark-5 dark:text-gray-300">Upload Recommit Payment Proof</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach

        {{-- Payer's Recommit transactions waiting for receiver approval --}}
        @foreach ($recommitApprovalTransactions as $transaction)
            {{-- Show when user transaction is waiting for approval --}}
            <div class="intro-y box p-5 bg-theme-1 text-white mt-5">
                <div class="flex items-center">
                    <div class="font-small text-md">
                        <p>You have uploaded your recommit payment proof, please wait for approval.</p><br>
                        
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Amount Sent</span> R{{$transaction->transaction_amount}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">To</span> {{$transaction->name}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Bank Name</span> {{$transaction->bank_name}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Bank Account</span> {{$transaction->acc_number}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Phone Number</span> {{$transaction->phone_number}}</span> <br><br>
                        <div class="font-medium flex mt-5">
                            <button class="button button--sm border border-white text-white dark:border-dark-5 dark:text-gray-300">Recommit Waiting for Approval</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        {{-- Receiver's Recommit transactions waiting to be approved --}}
        @foreach ($recommitApproverTransactions as $transaction)
            {{-- Show when user transaction is waiting for approval --}}
            <div class="intro-y box p-5 bg-theme-1 text-white mt-5">
                <div class="flex items-center">
                    <div class="font-small text-md">
                        
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Amount Received</span> R{{$transaction->transaction_amount}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">From</span>{{$transaction->name}}</span> <br><br>
                        <span style="background-color: black;color:white;padding:5px;border-radius:4px;"><span style="padding:5px;border-radius:4px;background-color: rgb(243, 243, 243);color:black">Phone Number</span> {{$transaction->phone_number}}</span> <br><br>
                        <p>Click approve if you have received R{{$transaction->transaction_amount}} from {{$transaction->name}}</p>
                        <p>If didn't receive the money, contact {{$transaction->name}} on {{$transaction->phone_number}}</p>
                        <form method="POST" action="{{ route('recommit') }}">
                            {{ csrf_field() }}
                        <div class="font-medium flex mt-5">
                            <input type="hidden" name="transaction_id" value="{{$transaction->transaction_id}}"> 
                            <input type="hidden" name="project_id" value="{{$transaction->project}}"> 
                            <input type="hidden" name="transaction_amount" value="{{$transaction->transaction_amount}}"> 
                            <input type="hidden" name="payer_id" value="{{$transaction->payer_id}}"> 
                            <button class="button button--sm border border-white text-white dark:border-dark-5 dark:text-gray-300">Approve Payment</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
        
    @endforeach

    
@endsection