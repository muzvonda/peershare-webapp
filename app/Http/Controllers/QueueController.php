<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Queue;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ProjectController;

class QueueController extends Controller
{

    public function fetchFirstPaymentQueue(){

        $firstPaymentQueue = Queue::where('queue_status','3')->get();
        
        return $firstPaymentQueue;

    }

    public function fetchSecondPaymentQueue(){

        $secondPaymentQueue = Queue::where('queue_status','4')->get();
        
        return $secondPaymentQueue;

    }

    public function fetchNextReceiver($payerPlan, $stokvel_id, $previousPayer){

        $paymentQueue = Queue::select('user_id','stokvel_id','plan_id','queue_status','created_at','updated_at')
        ->where('user_id', '!=',$previousPayer)
        ->where('queue_status','3')
        ->where('plan_id',$payerPlan)
        ->where('stokvel_id',$stokvel_id)
        ->orWhere('queue_status','4')
        ->where('plan_id',$payerPlan)
        ->where('stokvel_id',$stokvel_id)
        ->orderBy('updated_at')
        ->limit(1)
        ->first();

        return $paymentQueue;


    }

    //this function should only be called on transaction approval
    //payer queue status
    public function paymentMadeQueueUpdate($projectID, $payer_id, $type){

        //type 1 means its the payer's first payment on the project
        //type 2 means second payment

        //move payer user to queue status 2 if on 1 and to 3 if on 2, meaning they have to recommit
        //the current user when this is called is the receiver

        $getStokvelID = new ProjectController;
        $stokvelID = $getStokvelID->getStokvelID($projectID)->stokvel_id;
        $currentQueueStatus = Queue::where('stokvel_id',$stokvelID)
        ->where('user_id', $payer_id)->first();

        if($currentQueueStatus->queue_status == 1 && $type == 1){

        Queue::where('user_id',$payer_id)
        ->where('stokvel_id',$stokvelID)
        ->where('queue_status', 1)
        ->update(['queue_status' => 2]);

        }else if($currentQueueStatus->queue_status == 2 && $type == 2){

        Queue::where('user_id',$payer_id)
        ->where('stokvel_id',$stokvelID)
        ->where('queue_status', 2)
        ->update(['queue_status' => 3]);

        }

    }

    //receiver queue status
    public function paymentApprovedQueueUpdate($projectID){

        //move receiver user to queue status 4 if on 3, and to 1 if on 4, meaning they have to recommit
        //receiver is the current user when this function is called

        $getStokvelID = new ProjectController;
        $stokvelID = $getStokvelID->getStokvelID($projectID)->stokvel_id;

        $currentQueueStatus = Queue::where('stokvel_id',$stokvelID)
        ->where('user_id',Auth::id())->first();
        
        if($currentQueueStatus->queue_status == 3){

            Queue::where('user_id',Auth::id())
            ->where('stokvel_id', $stokvelID)
            ->where('queue_status', 3)
            ->update(['queue_status' => 4]);

        }else if($currentQueueStatus->queue_status == 4){

            Queue::where('user_id',Auth::id())
            ->where('stokvel_id',$stokvelID)
            ->where('queue_status', 4)
            ->update(['queue_status' => 1]);

        }

        
    }
    
}
