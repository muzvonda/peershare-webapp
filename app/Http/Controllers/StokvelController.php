<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stokvel;
use App\Models\StokvelMembership;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PeerDashboardController;
use App\Http\Controllers\PageController;

class StokvelController extends Controller
{
    public function saveStokvel(Request $request){


        $newStokvel = new Stokvel;
        $newStokvel->stokvel_name = $request->stokvel_name;
        $newStokvel->admins = Auth::id();
        $newStokvel->population_limit = $request->stokvel_capacity;
        $newStokvel->population = 0;
        $newStokvel->project_types =  json_encode($request->project_types);
        $newStokvel->save();

    }

    public function stokvels($layout = 'side-menu', $theme = 'light', $pageName = 'stokvels'){
        $peerDash = new PeerDashboardController;
        $activeMenuType = new PageController;
        $activeMenu = $activeMenuType->activeMenu($layout, $pageName);
        $checkUserReg = $peerDash->checkRegFeeStatus();

        $stokvels = $this->getUserStokvels();

        if( $checkUserReg != 1 ){
            return redirect(route('registrationfee'));
        }else{

            return view('pages/'.$pageName, 
                [ 
                    'layout' => $layout,
                    'theme' => $theme,
                    'first_page_name' => $activeMenu['first_page_name'],
                    'second_page_name' => $activeMenu['second_page_name'],
                    'third_page_name' => $activeMenu['third_page_name'],
                    'page_name' => $pageName,
                    'side_menu' => $peerDash->peerSideMenu(),
                    'stokvels' => $stokvels,
                    ]
            );
        }

    }

    public function communityStokvels($layout = 'side-menu', $theme = 'light', $pageName = 'community-stokvels'){
        $peerDash = new PeerDashboardController;
        $activeMenuType = new PageController;
        $activeMenu = $activeMenuType->activeMenu($layout, $pageName);
        $checkUserReg = $peerDash->checkRegFeeStatus();

        $stokvels = $this->getCommunityStokvels();

        if( $checkUserReg != 1 ){
            return redirect(route('registrationfee'));
        }else{

            return view('pages/'.$pageName, 
                [ 
                    'layout' => $layout,
                    'theme' => $theme,
                    'first_page_name' => $activeMenu['first_page_name'],
                    'second_page_name' => $activeMenu['second_page_name'],
                    'third_page_name' => $activeMenu['third_page_name'],
                    'page_name' => $pageName,
                    'side_menu' => $peerDash->peerSideMenu(),
                    'stokvels' => $stokvels,
                    ]
            );
        }

    }

    public function getAllStokvelDetails(){

        $stokvelDetails = Stokvel::leftJoin('users', 'stokvels.admins', '=', 'users.id')
            ->leftJoin('project_types', 'stokvels.admins', '=', 'users.id')
            ->get();

        return $stokvelDetails;

    }

    public function getAllStokvels(){

        $allStockvels = Stokvel::all();

        return $allStockvels;

    }

    public function getCommunityStokvels(){

        $userUnjoinedStokvels = Stokvel::
        leftJoin('stokvel_memberships','stokvels.id', '=', 'stokvel_memberships.stokvel_id')
        ->select('stokvels.stokvel_name','stokvels.id','stokvels.population','stokvels.population_limit', 'stokvels.project_types')
        ->where('user_id','!=',Auth::id())
        ->orWhereNull('user_id')
        ->get();

        return $userUnjoinedStokvels;

    }

    public function getUserStokvels(){
        
        $userStokvels = StokvelMembership::leftJoin('stokvels','stokvel_memberships.stokvel_id', '=', 'stokvels.id')
        ->where('user_id', Auth::id())
        ->get();

        return $userStokvels;
    }

    public function createStokvel($layout = 'side-menu', $theme = 'light', $pageName = 'create-stokvel'){
        $peerDash = new PeerDashboardController;
        $activeMenuType = new PageController;
        $checkUserReg = $peerDash->checkRegFeeStatus();
        
        $activeMenu = $activeMenuType->activeMenu($layout, $pageName);

        if( $checkUserReg != 1 ){
            return redirect(route('registrationfee'));
        }else{

            return view('pages/'.$pageName, 
                [ 
                    'layout' => $layout,
                    'theme' => $theme,
                    'first_page_name' => $activeMenu['first_page_name'],
                    'second_page_name' => $activeMenu['second_page_name'],
                    'third_page_name' => $activeMenu['third_page_name'],
                    'page_name' => $pageName,
                    'side_menu' => $peerDash->peerSideMenu() 
                    ]
            );
        }

    }

    public function joinStokvel(Request $request){
        $stokvelMember = new StokvelMembership;
        $stokvelMember->user_id = Auth::id();
        $stokvelMember->stokvel_id = $request->stokvel_id;
        $stokvelMember->stokvel_name = $request->stokvel_name;
        $stokvelMember->member_type = 'member';
        $stokvelMember->membership_status = 1;
        $stokvelMember->save();

        return redirect(route('stokvels'));

    }
}
