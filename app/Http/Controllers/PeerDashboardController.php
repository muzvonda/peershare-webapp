<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Auth;
use App\Models\Referral;
use App\Models\User;

class PeerDashboardController extends Controller
{
    public function index($layout = 'side-menu', $theme = 'light', $pageName = 'peer-dashboard'){
        $activeMenuType = new PageController;
        
        $activeMenu = $activeMenuType->activeMenu($layout, $pageName);

        $checkUserReg = $this->checkRegFeeStatus();

        // if( $checkUserReg != 1 ){
        //     return redirect(route('registrationfee'));
        // }else{
        
            return view('pages/'.$pageName, 
                [ 
                    'layout' => $layout,
                    'theme' => $theme,
                    'first_page_name' => $activeMenu['first_page_name'],
                    'second_page_name' => $activeMenu['second_page_name'],
                    'third_page_name' => $activeMenu['third_page_name'],
                    'page_name' => $pageName,
                    'side_menu' => $this->peerSideMenu() 
                    ]
            );
        // }

    }

    public function checkRegFeeStatus(){

        $userDetails = Auth::user();

        return $userDetails->reg_fee;

    }

    public function registrationFee($layout = 'side-menu', $theme = 'light', $pageName = 'registration-fee'){
        $activeMenuType = new PageController;
        $userDetails = Auth::user();
        $activeMenu = $activeMenuType->activeMenu($layout, $pageName);
        $checkUserReg = $this->checkRegFeeStatus();

        return view('pages/'.$pageName, 
            [ 
                'layout' => $layout,
                'theme' => $theme,
                'first_page_name' => $activeMenu['first_page_name'],
                'second_page_name' => $activeMenu['second_page_name'],
                'third_page_name' => $activeMenu['third_page_name'],
                'page_name' => $pageName,
                'side_menu' => $this->peerSideMenu(),
                'checkUserReg' => $checkUserReg,
                'userEmail' => $userDetails->email,
                ]
        );

    }

    public function referrals($layout = 'side-menu', $theme = 'light', $pageName = 'referrals'){
        $activeMenuType = new PageController;
        $currentUser = Auth::user();
        $userNumber = $currentUser->phone_number;

        $userReferrals = User::where('referral_code', $userNumber)->get();
        $activeReferrals = User::where('referral_code', $userNumber)
        ->where('reg_fee',1)
        ->get();
        $referralBalance = 5*$activeReferrals->count();

        $activeMenu = $activeMenuType->activeMenu($layout, $pageName);

        return view('pages/'.$pageName, 
            [ 
                'layout' => $layout,
                'theme' => $theme,
                'first_page_name' => $activeMenu['first_page_name'],
                'second_page_name' => $activeMenu['second_page_name'],
                'third_page_name' => $activeMenu['third_page_name'],
                'page_name' => $pageName,
                'side_menu' => $this->peerSideMenu() ,
                'referralLink' => $userNumber,
                'userReferrals' => $userReferrals,
                'referralBalance' => $referralBalance,
                ]
        );

    }

    public function profile($layout = 'side-menu', $theme = 'light', $pageName = 'profile'){
        $activeMenuType = new PageController;
        
        $activeMenu = $activeMenuType->activeMenu($layout, $pageName);

        return view('pages/'.$pageName, 
            [ 
                'layout' => $layout,
                'theme' => $theme,
                'first_page_name' => $activeMenu['first_page_name'],
                'second_page_name' => $activeMenu['second_page_name'],
                'third_page_name' => $activeMenu['third_page_name'],
                'page_name' => $pageName,
                'side_menu' => $this->peerSideMenu() 
                ]
        );

    }

        /**
     * List of side menu items.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function peerSideMenu()
    {
        return [
            'dashboard' => [
                'icon' => 'home',
                'layout' => 'side-menu',
                'page_name' => 'home',
                'title' => 'Dashboard'
            ],
            'devider',
            'projects' => [
                'icon' => 'box',
                'layout' => 'simple-menu',
                'page_name' => 'projects',
                'title' => 'My Projects'
            ],
            'devider',
            'stokvels' => [
                'icon' => 'database',
                'layout' => 'simple-menu',
                'page_name' => 'stokvels',
                'title' => 'Stokvels'
            ],
            'devider',
            'referrals' => [
                'icon' => 'git-merge',
                'layout' => 'simple-menu',
                'page_name' => 'referrals',
                'title' => 'Referrals'
            ],
            'devider',
            // 'profile' => [
            //     'icon' => 'user',
            //     'layout' => 'simple-menu',
            //     'page_name' => 'profile',
            //     'title' => 'My Profile'
            // ],
        ];
    }
}
