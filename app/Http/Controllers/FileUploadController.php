<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;
use App\Http\Controllers\QueueController;

class FileUploadController extends Controller
{

    public function fileUpload()
    {
        return view('fileUpload');
    }
  

    public function fileUploadPost(Request $request)
    {

        $fileData = new File;
        $fileData->project_id = $request->project_id;
        $fileData->transaction_id = $request->transaction_id;
        $fileData->transaction_amount = $request->transaction_amount;
        $fileData->payer_id = $request->payer_id;
        $fileData->receiver_id = $request->receiver_id;
        $fileData->save();

        $request->validate([
            'file' => 'required|mimes:pdf,jpg,jpeg,png|max:2048',
        ]);

        $fileName = $request->transaction_id.'.'.$request->file->extension();
        $file = $request->file('file');
        $destinationPath = 'uploads';
        $file->move($destinationPath,$fileName);

        $transactionStatus = new TransactionsController;

        if($request->payment == 'first'){

            $transactionStatus->paymentMadeTransaction($request->project_id, 1);

        }else if($request->payment == 'second'){

            $transactionStatus->paymentMadeTransaction($request->project_id, 2);

        }

       

        return back()
        ->with('success','You have successfully upload your payment proof.')
        ->with('file',$file->getClientOriginalName());
   
    }
}
