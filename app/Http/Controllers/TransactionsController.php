<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Transactions;
use App\Models\Plan;
use App\Http\Controllers\QueueController;
use App\Models\Project;
use App\Http\Controllers\ProjectController;

class TransactionsController extends Controller
{
    public function createTransaction($planAmount,$receiverID,$savedProject)
    {

        $newTransaction = new Transactions;
        $newTransaction->project = $savedProject;
        $newTransaction->transaction_amount = $planAmount;
        $newTransaction->payer_id = Auth::id();
        $newTransaction->receiver_id = $receiverID;
        $newTransaction->transaction_type_id = 1;
        $newTransaction->transaction_status = 1;
        $newTransaction->save();

        return $newTransaction;

    }

    //payer transactions, not yet paid
    public function fetchPayingUserTransaction(){

        $payer_id = Auth::id();
        $transactions = Transactions::
        select('users.id as users_id', 'transactions.id as transaction_id', 
        'transactions.payer_id as payer_id', 'transactions.transaction_amount as transaction_amount', 
        'transactions.transaction_status as transaction_status', 'transactions.receiver_id as receiver_id', 
        'transactions.project as project', 'transactions.transaction_type_id as transaction_type_id', 
        'users.name as name', 'users.bank_name as bank_name', 'users.acc_number as acc_number', 
        'users.phone_number as phone_number' )
        ->leftJoin('users','transactions.receiver_id', '=', 'users.id')
        ->where( 'payer_id',$payer_id )
        ->where('transaction_type_id',1)
        ->where('transaction_status', 1)
        ->get();

        return $transactions;

    }

    //receiver's transactions, not yet paid
    public function fetchReceiverUserTransaction(){

        $receiver_id = Auth::id();
        $transactions = Transactions::
        select('users.id as users_id', 'transactions.id as transaction_id', 
        'transactions.payer_id as payer_id', 'transactions.transaction_amount as transaction_amount', 
        'transactions.transaction_status as transaction_status', 'transactions.receiver_id as receiver_id', 
        'transactions.project as project', 'transactions.transaction_type_id as transaction_type_id', 
        'users.name as name', 'users.bank_name as bank_name', 'users.acc_number as acc_number', 
        'users.phone_number as phone_number' )
        ->leftJoin('users','transactions.payer_id', '=', 'users.id')
        ->where( 'receiver_id',$receiver_id )
        ->where('transaction_type_id',1)
        ->where('transaction_status', 1)
        ->get();

        return $transactions;

    }

    //payer recommit transactions, not yet paid
    public function fetchRecommitTransactions(){

        $payer_id = Auth::id();
        $recommitTransactions = Transactions::
        select('users.id as users_id', 'transactions.id as transaction_id', 
        'transactions.payer_id as payer_id', 'transactions.transaction_amount as transaction_amount', 
        'transactions.transaction_status as transaction_status', 'transactions.receiver_id as receiver_id', 
        'transactions.project as project', 'transactions.transaction_type_id as transaction_type_id', 
        'users.name as name', 'users.bank_name as bank_name', 'users.acc_number as acc_number', 
        'users.phone_number as phone_number' )
        ->leftJoin('users','transactions.receiver_id', '=', 'users.id')
        ->where( 'payer_id',$payer_id )
        ->where('transaction_type_id',2)
        ->where('transaction_status', 1)
        ->get();

        return $recommitTransactions;

    }

    //receiver recommit transactions, not yet paid
    public function fetchReceiverRecommitTransactions(){

        $receiver_id = Auth::id();
        $recommitTransactions = Transactions::
        select('users.id as users_id', 'transactions.id as transaction_id', 
        'transactions.payer_id as payer_id', 'transactions.transaction_amount as transaction_amount', 
        'transactions.transaction_status as transaction_status', 'transactions.receiver_id as receiver_id', 
        'transactions.project as project', 'transactions.transaction_type_id as transaction_type_id', 
        'users.name as name', 'users.bank_name as bank_name', 'users.acc_number as acc_number', 
        'users.phone_number as phone_number' )
        ->leftJoin('users','transactions.receiver_id', '=', 'users.id')
        ->where( 'receiver_id',$receiver_id )
        ->where('transaction_type_id',2)
        ->where('transaction_status', 1)
        ->get();

        return $recommitTransactions;

    }

    //Call this method on payment proof uploaded
    public function paymentMadeTransaction($projectID, $payment){

        if($payment == 1){

            //update transaction to 2 : which means waiting for approval
            Transactions::where('payer_id',Auth::id())
            ->where('project',$projectID)
            ->where('transaction_status',1)
            ->where('transaction_type_id',1)
            ->update(['transaction_status' => 2]);

        }else if($payment == 2){

            Transactions::where('payer_id',Auth::id())
            ->where('project',$projectID)
            ->where('transaction_status',1)
            ->where('transaction_type_id',2)
            ->update(['transaction_status' => 2]);

        }
       

    }

    public function fetchApprovalTransactions(){

        $payer_id = Auth::id();
        $approvalTransactions = Transactions::
        select('users.id as users_id', 'transactions.id as transaction_id', 
        'transactions.payer_id as payer_id', 'transactions.transaction_amount as transaction_amount', 
        'transactions.transaction_status as transaction_status', 'transactions.receiver_id as receiver_id', 
        'transactions.project as project', 'transactions.transaction_type_id as transaction_type_id', 
        'users.name as name', 'users.bank_name as bank_name', 'users.acc_number as acc_number', 
        'users.phone_number as phone_number' )
        ->leftJoin('users','transactions.receiver_id', '=', 'users.id')
        ->where( 'payer_id',$payer_id )
        ->where('transaction_type_id',1)
        ->where('transaction_status', 2)
        ->get();

        return $approvalTransactions;

    }

    public function fetchApproverTransactions(){

        $receiver_id = Auth::id();
        $approvalTransactions = Transactions::
        select('users.id as users_id', 'transactions.id as transaction_id', 
        'transactions.payer_id as payer_id', 'transactions.transaction_amount as transaction_amount', 
        'transactions.transaction_status as transaction_status', 'transactions.receiver_id as receiver_id', 
        'transactions.project as project', 'transactions.transaction_type_id as transaction_type_id', 
        'users.name as name', 'users.bank_name as bank_name', 'users.acc_number as acc_number', 
        'users.phone_number as phone_number' )
        ->leftJoin('users','transactions.payer_id', '=', 'users.id')
        ->where( 'receiver_id',$receiver_id )
        ->where('transaction_type_id',1)
        ->where('transaction_status', 2)
        ->get();

        return $approvalTransactions;

    }

    public function fetchRecommitApprovalTransactions(){

        $payer_id = Auth::id();
        $approvalTransactions = Transactions::
        select('users.id as users_id', 'transactions.id as transaction_id', 
        'transactions.payer_id as payer_id', 'transactions.transaction_amount as transaction_amount', 
        'transactions.transaction_status as transaction_status', 'transactions.receiver_id as receiver_id', 
        'transactions.project as project', 'transactions.transaction_type_id as transaction_type_id', 
        'users.name as name', 'users.bank_name as bank_name', 'users.acc_number as acc_number', 
        'users.phone_number as phone_number' )
        ->leftJoin('users','transactions.receiver_id', '=', 'users.id')
        ->where( 'payer_id',$payer_id )
        ->where('transaction_type_id',2)
        ->where('transaction_status', 2)
        ->get();

        return $approvalTransactions;

    }

    //receiver recommit transactions
    public function fetchRecommitApproverTransactions(){

        $receiver_id = Auth::id();
        $recommitApprovalTransactions = Transactions::
        select('users.id as users_id', 'transactions.id as transaction_id', 
        'transactions.payer_id as payer_id', 'transactions.transaction_amount as transaction_amount', 
        'transactions.transaction_status as transaction_status', 'transactions.receiver_id as receiver_id', 
        'transactions.project as project', 'transactions.transaction_type_id as transaction_type_id', 
        'users.name as name', 'users.bank_name as bank_name', 'users.acc_number as acc_number', 
        'users.phone_number as phone_number' )
        ->leftJoin('users','transactions.payer_id', '=', 'users.id')
        ->where( 'receiver_id',$receiver_id )
        ->where('transaction_type_id',2)
        ->where('transaction_status', 2)
        ->get();

        return $recommitApprovalTransactions;

    }

    //this function is called on transaction approval
    public function approveTransaction(Request $request){

        $queueStatus = new QueueController; 
        $projectDetail = new ProjectController;

        //Payer queue status update to 2:recommit or 3:receive
        $queueStatus->paymentMadeQueueUpdate($request->project_id, $request->payer_id, 1); //checked-working

        //Receiver queue status update to 4:receiveSecondPayment or 1:paySomeone
        $queueStatus->paymentApprovedQueueUpdate($request->project_id); //checked-working

        //Transaction status update to 3:Approved
        Transactions::where('id',$request->transaction_id)->update(['transaction_status' => 3]); //checked-working

        //generate the recommit transaction for payer when the 1st transaction is approved
        $getStokvelID = $projectDetail->getStokvelID($request->project_id);

        $stokvelID = $getStokvelID->stokvel_id; //changed from ['stokvel_id']
        $planId = Plan::where('plan_amount', $request->transaction_amount)->first();

        $receiver = $queueStatus->fetchNextReceiver($planId->id,$stokvelID,$request->payer_id);

        if(!empty($receiver)){
        $this->createTransaction( $request->transaction_amount, $receiver->user_id ,$request->project_id);
        }

        return back();

    }

        //this function is called on recommit transaction approval
        public function approveRecommitTransaction(Request $requestR){

            $queueStatusR = new QueueController; 
            $projectDetailR = new ProjectController;
    
            //Payer queue status update to  3:payerFirstPayment
            $queueStatusR->paymentMadeQueueUpdate($requestR->project_id, $requestR->payer_id, 2); //checked
    
            //Receiver queue status update to 4:receiveSecondPayment or 1:paySomeone
            $queueStatusR->paymentApprovedQueueUpdate($requestR->project_id); //checked
    
            //Transaction status update to 3:Approved
            Transactions::where('id',$requestR->transaction_id)->update(['transaction_status' => 3]); //checked
    
            //generate the recommit transaction for payer when the 1st transaction is approved
            $getStokvelIDR = $projectDetailR->getStokvelID($requestR->project_id);

            $stokvelIDR = $getStokvelIDR->stokvel_id; //changed from ['stokvel_id']
            $planIdR = Plan::where('plan_amount', $requestR->transaction_amount)->first();

            $receiver = $queueStatusR->fetchNextReceiver($planIdR->id,$stokvelIDR,$requestR->payer_id);

            if(!empty($receiver)){
                $this->createTransaction( $requestR->transaction_amount, $receiver->user_id ,$requestR->project_id);
            }
            
            return back();
    
        }

}
