<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Project;
use App\Models\ProjectType;
use App\Models\Plan;
use App\Models\ProjectStructure;
use App\Models\Queue;
use App\Http\Controllers\PeerDashboardController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\StokvelController;
use App\Http\Controllers\QueueController;
use App\Http\Controllers\TransactionsController;
use App\Models\Stokvel;

class ProjectController extends Controller
{

    public function index($layout = 'side-menu', $theme = 'light', $pageName = 'projects'){
        $peerDash = new PeerDashboardController;
        $activeMenuType = new PageController;
        $activeMenu = $activeMenuType->activeMenu($layout, $pageName);
        $checkUserReg = $peerDash->checkRegFeeStatus();
        $paymentQueue = new QueueController;
        $planAmount = $this->getPlanAmount(2)->plan_amount;
        $allProjects = $this->getAllProjects();
        $userProjects = $this->getUserProjects();

        if( $checkUserReg != 1 ){
            return redirect(route('registrationfee'));
        }else{

            return view('pages/'.$pageName, 
                [ 
                    'layout' => $layout,
                    'theme' => $theme,
                    'first_page_name' => $activeMenu['first_page_name'],
                    'second_page_name' => $activeMenu['second_page_name'],
                    'third_page_name' => $activeMenu['third_page_name'],
                    'page_name' => $pageName,
                    'side_menu' => $peerDash->peerSideMenu(),
                    'allProjects' => $allProjects,
                    'userProjects' => $userProjects,
                    'planAmount' => $planAmount,
                    ]
            );
        }

    }

    public function viewProject($project_id){
        $pageName = 'view-project';
        $theme = 'light';
        $layout = 'side-menu';
        $peerDash = new PeerDashboardController;
        $activeMenuType = new PageController;
        $activeMenu = $activeMenuType->activeMenu($layout, $pageName);
        $checkUserReg = $peerDash->checkRegFeeStatus();
        $projectDetails = Project::where( 'id', $project_id )->get();

        $userTransactionsFetch = new TransactionsController;

        //fetch payer's transactions
        $paymentTransactions = $userTransactionsFetch->fetchPayingUserTransaction();
        $recommitTransactions = $userTransactionsFetch->fetchRecommitTransactions();

        //fetch payer's transactions to be approved
        $approvalTransactions = $userTransactionsFetch->fetchApprovalTransactions();
        $recommitApprovalTransactions = $userTransactionsFetch->fetchRecommitApprovalTransactions();

        //fetch receiver's transactions to be approved
        $approverTransactions = $userTransactionsFetch->fetchApproverTransactions();
        $recommitApproverTransactions = $userTransactionsFetch->fetchRecommitApproverTransactions();

        //fetch receiver's transactions
        $receiverTransactions = $userTransactionsFetch->fetchReceiverUserTransaction();
        $receiverCommitTransactions = $userTransactionsFetch->fetchReceiverRecommitTransactions();


        $allProjects = $this->getAllProjects();
        $userProjects = $this->getUserProjects();

        if( $checkUserReg != 1 ){
            return redirect(route('registrationfee'));
        }else{

            return view('pages/'.$pageName, 
                [ 
                    'layout' => $layout,
                    'theme' => $theme,
                    'first_page_name' => $activeMenu['first_page_name'],
                    'second_page_name' => $activeMenu['second_page_name'],
                    'third_page_name' => $activeMenu['third_page_name'],
                    'page_name' => $pageName,
                    'side_menu' => $peerDash->peerSideMenu(),
                    'allProjects' => $allProjects,
                    'userProjects' => $userProjects,
                    'projectDetails'  => $projectDetails,
                    'paymentTransactions' => $paymentTransactions,
                    'approvalTransactions' => $approvalTransactions,
                    'approverTransactions' => $approverTransactions,
                    'recommitTransactions' => $recommitTransactions,
                    'recommitApprovalTransactions' => $recommitApprovalTransactions,
                    'recommitApproverTransactions' => $recommitApproverTransactions,
                    'receiverTransactions' => $receiverTransactions,
                    'receiverCommitTransactions' => $receiverCommitTransactions,

                    ]
            );
        }

    }

    public function createProject($layout = 'side-menu', $theme = 'light', $pageName = 'create-project'){
        $peerDash = new PeerDashboardController;
        $activeMenuType = new PageController;
        $stokvelListType = new StokvelController;

        $projectTypes = $this->getAllProjectsTypes();
        $projectStructures = $this->getAllProjectsStructures();
        $plans = $this->getAllPlans();
        $checkUserReg = $peerDash->checkRegFeeStatus();

        $allStokvels = $stokvelListType->getAllStokvels();
        $activeMenu = $activeMenuType->activeMenu($layout, $pageName);

        if( $checkUserReg != 1 ){
            return redirect(route('registrationfee'));
        }else{

            return view('pages/'.$pageName, 
                [ 
                    'layout' => $layout,
                    'theme' => $theme,
                    'first_page_name' => $activeMenu['first_page_name'],
                    'second_page_name' => $activeMenu['second_page_name'],
                    'third_page_name' => $activeMenu['third_page_name'],
                    'page_name' => $pageName,
                    'side_menu' => $peerDash->peerSideMenu(),
                    'allStokvels' => $allStokvels,
                    'projectTypes' => $projectTypes,
                    'projectStructures' => $projectStructures,
                    'plans' => $plans,
                    ]
            );
        }

    }

    public function saveProject(Request $request){

        $newProject = new Project;
        $newProject->project_name = $request->project_name ;
        $newProject->user_id = Auth::id();
        $newProject->stokvel_id = $request->stokvel_id;
        $newProject->project_type_id = $request->project_type_id;
        $newProject->project_structure_id = $request->project_structure_id;
        $newProject->plan_id = $request->plan_id;
        $newProject->goal = $request->goal;
        $newProject->save();
        $savedProject = $newProject->id;

        $createQueue = new Queue;
        $createQueue->user_id = Auth::id();
        $createQueue->stokvel_id = $request->stokvel_id;
        $createQueue->plan_id = $request->plan_id;
        $createQueue->queue_status = 1;
        $createQueue->save();

        $allocateReceiver = new QueueController;
        $receiver = $allocateReceiver->fetchNextReceiver($request->plan_id,$request->stokvel_id, Auth::id());
        
        $receiver_id = $receiver['user_id'];
        $planAmount = $this->getPlanAmount($request->plan_id)->plan_amount;

        $generateTransaction = new TransactionsController;

        if(!empty($receiver)){
            $generateTransaction->createTransaction( $planAmount, $receiver_id ,$savedProject);
        }
        

        return redirect(route('projects'));

    }

    public function getAllProjects(){

        $allProjects = Project::all();

        return $allProjects;

    }

    public function getUserProjects(){

        $allProjects = Project::where('user_id',Auth::id())->get();

        return $allProjects;

    }

    public function getAllProjectsTypes(){

        $allProjectsTypes = ProjectType::all();

        return $allProjectsTypes;

    }

    public function getAllProjectsStructures(){

        $allProjectsStructures = ProjectStructure::all();

        return $allProjectsStructures;

    }

    public function getAllPlans(){

        $allPlans = Plan::all();

        return $allPlans;

    }

    public function getPlanAmount($planID){

        $planAmount = Plan::where('id',$planID)->find($planID);

        return $planAmount;

    }

    public function getStokvelID($projectID){

        $stokvelID = Project::where('id',$projectID)->first();

        return $stokvelID;
    }

}
