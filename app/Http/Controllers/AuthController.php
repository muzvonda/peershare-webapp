<?php

namespace App\Http\Controllers;

use App\Http\Request\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Referral;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loginView()
    {
        return view('login/main', [
            'theme' => 'light',
            'page_name' => 'auth-login',
            'layout' => 'login'
        ]);
    }

        /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registerView()
    {

        return view('pages/register', [
            'theme' => 'light',
            'page_name' => 'auth-login',
            'layout' => 'login',
        ]);
    }

    /**
     * Authenticate login user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        if (!\Auth::attempt([
            'email' => $request->email, 
            'password' => $request->password
        ])) {
            throw new \Exception('Wrong email or password.');
        }
    }

    public function register(Request $request){

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->bank_name = $request->bank_name;
        $user->acc_number = $request->acc_number;
        $user->phone_number = $request->phone_number;
        $user->referral_code = $request->referral_code;
        $user->password = Hash::make($request->password);
        $user->active = 1;
        $user->reg_fee = 0;
        $user->save();

        $savedUserId = $user->id; //take the user_id of the newly saved user.

        if(!empty($request->referral_code)){

            $referrerId = User::where('phone_number',$request->referral_code)->first();

            $referral = new Referral;
            $referral->user_id = $savedUserId;
            $referral->referrer_id = $referrerId->id;
            $referral->code = $request->referral_code;
            $referral->save();
        }

        return redirect('login');

    }

    /**
     * Logout user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        \Auth::logout();
        return redirect('login');
    }
}
