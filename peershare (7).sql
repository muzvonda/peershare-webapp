-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2022 at 08:04 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `peershare`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `transaction_amount` int(11) NOT NULL,
  `payer_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2014_10_12_000000_create_users_table', 1),
(32, '2014_10_12_100000_create_password_resets_table', 1),
(33, '2019_08_19_000000_create_failed_jobs_table', 1),
(34, '2022_01_21_153648_create_stokvels_table', 1),
(35, '2022_01_21_153704_create_projects_table', 1),
(36, '2022_01_21_153745_create_transactions_table', 1),
(37, '2022_01_21_164902_create_transaction_types_table', 1),
(38, '2022_01_21_164958_create_project_types_table', 1),
(39, '2022_01_21_165153_create_plans_table', 1),
(40, '2022_01_21_165318_create_project_structures_table', 1),
(41, '2014_10_12_000000_create_users_table', 1),
(42, '2014_10_12_100000_create_password_resets_table', 1),
(43, '2019_08_19_000000_create_failed_jobs_table', 1),
(44, '2022_01_21_153648_create_stokvels_table', 1),
(45, '2022_01_21_153704_create_projects_table', 1),
(46, '2022_01_21_153745_create_transactions_table', 1),
(47, '2022_01_21_164902_create_transaction_types_table', 1),
(48, '2022_01_21_164958_create_project_types_table', 1),
(49, '2022_01_21_165153_create_plans_table', 1),
(50, '2022_01_21_165318_create_project_structures_table', 1),
(86, '2014_10_12_000000_create_users_table', 1),
(87, '2014_10_12_100000_create_password_resets_table', 1),
(88, '2019_08_19_000000_create_failed_jobs_table', 1),
(89, '2022_01_21_153648_create_stokvels_table', 1),
(90, '2022_01_21_153704_create_projects_table', 1),
(91, '2022_01_21_153745_create_transactions_table', 1),
(92, '2022_01_21_164902_create_transaction_types_table', 1),
(93, '2022_01_21_164958_create_project_types_table', 1),
(94, '2022_01_21_165153_create_plans_table', 1),
(95, '2022_01_21_165318_create_project_structures_table', 1),
(107, '2014_10_12_000000_create_users_table', 1),
(108, '2014_10_12_100000_create_password_resets_table', 1),
(109, '2019_08_19_000000_create_failed_jobs_table', 1),
(110, '2022_01_21_153648_create_stokvels_table', 1),
(111, '2022_01_21_153704_create_projects_table', 1),
(112, '2022_01_21_153745_create_transactions_table', 1),
(113, '2022_01_21_164902_create_transaction_types_table', 1),
(114, '2022_01_21_164958_create_project_types_table', 1),
(115, '2022_01_21_165153_create_plans_table', 1),
(116, '2022_01_21_165318_create_project_structures_table', 1),
(145, '2014_10_12_000000_create_users_table', 1),
(146, '2014_10_12_100000_create_password_resets_table', 1),
(147, '2019_08_19_000000_create_failed_jobs_table', 1),
(148, '2022_01_21_153648_create_stokvels_table', 1),
(149, '2022_01_21_153704_create_projects_table', 1),
(150, '2022_01_21_153745_create_transactions_table', 1),
(151, '2022_01_21_164902_create_transaction_types_table', 1),
(152, '2022_01_21_164958_create_project_types_table', 1),
(153, '2022_01_21_165153_create_plans_table', 1),
(154, '2022_01_21_165318_create_project_structures_table', 1),
(163, '2022_01_22_013059_create_queues_table', 2),
(164, '2022_01_22_212348_create_stokvel_memberships_table', 2),
(165, '2022_01_23_154217_create_files_table', 2),
(166, '2022_01_28_155632_create_referrals_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `plan_amount` int(11) NOT NULL,
  `plan_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `plan_amount`, `plan_status`, `created_at`, `updated_at`) VALUES
(1, 100, 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08'),
(2, 200, 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08'),
(3, 500, 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08'),
(4, 800, 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08'),
(5, 1200, 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08'),
(6, 1500, 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `stokvel_id` int(11) NOT NULL,
  `project_type_id` int(11) NOT NULL,
  `project_structure_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `goal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `user_id`, `stokvel_id`, `project_type_id`, `project_structure_id`, `plan_id`, `goal`, `created_at`, `updated_at`) VALUES
(14, 'Muz Project', 1, 1, 1, 1, 2, 10000, '2022-01-26 06:12:43', '2022-01-26 06:12:43'),
(15, 'IT Project', 3, 1, 1, 1, 2, 15000, '2022-01-26 06:16:48', '2022-01-26 06:16:48'),
(25, 'The Famous', 2, 1, 1, 1, 2, 20000, '2022-01-28 08:23:56', '2022-01-28 08:23:56'),
(27, 'Lewin\'s Project', 1, 1, 1, 1, 2, 10000, '2022-01-28 08:47:09', '2022-01-28 08:47:09'),
(28, 'Vayakos', 1, 1, 1, 1, 2, 100000, '2022-01-28 08:48:14', '2022-01-28 08:48:14'),
(29, 'Enda', 3, 1, 2, 1, 2, 15000, '2022-01-28 08:49:54', '2022-01-28 08:49:54'),
(30, 'Test Project', 1, 1, 1, 1, 1, 5000, '2022-01-28 09:08:40', '2022-01-28 09:08:40'),
(31, 'TAPNOD', 1, 1, 1, 1, 4, 10000, '2022-01-28 09:11:14', '2022-01-28 09:11:14'),
(32, 'Len', 1, 1, 1, 1, 6, 10000, '2022-01-28 09:11:39', '2022-01-28 09:11:39'),
(33, 'ABC', 2, 1, 1, 1, 5, 10000, '2022-01-28 09:13:56', '2022-01-28 09:13:56'),
(34, 'AAC', 2, 1, 1, 1, 3, 10000, '2022-01-28 09:15:36', '2022-01-28 09:15:36');

-- --------------------------------------------------------

--
-- Table structure for table `project_structures`
--

CREATE TABLE `project_structures` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_structure_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_structures`
--

INSERT INTO `project_structures` (`id`, `project_structure_name`, `created_at`, `updated_at`) VALUES
(1, 'Peer to Peer', '2022-01-21 16:00:08', '2022-01-21 16:00:08'),
(2, 'Central Deposit', '2022-01-21 16:00:08', '2022-01-21 16:00:08');

-- --------------------------------------------------------

--
-- Table structure for table `project_types`
--

CREATE TABLE `project_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_type_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_type_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_types`
--

INSERT INTO `project_types` (`id`, `project_type_name`, `project_type_status`, `created_at`, `updated_at`) VALUES
(1, 'Savings', 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08'),
(2, 'Funeral', 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08'),
(3, 'School Fees', 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08'),
(4, 'Housing', 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08'),
(5, 'Wedding', 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08'),
(6, 'Travel', 1, '2022-01-21 16:00:08', '2022-01-21 16:00:08');

-- --------------------------------------------------------

--
-- Table structure for table `queues`
--

CREATE TABLE `queues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `stokvel_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `queue_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `queues`
--

INSERT INTO `queues` (`id`, `user_id`, `stokvel_id`, `plan_id`, `queue_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 3, NULL, NULL),
(2, 2, 1, 2, 4, NULL, NULL),
(3, 3, 1, 2, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stokvels`
--

CREATE TABLE `stokvels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `stokvel_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admins` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `population_limit` int(11) NOT NULL,
  `population` int(11) NOT NULL,
  `project_types` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stokvels`
--

INSERT INTO `stokvels` (`id`, `stokvel_name`, `admins`, `population_limit`, `population`, `project_types`, `created_at`, `updated_at`) VALUES
(1, 'PeerShare Stokvel', '1', 100, 0, '[\"Savings\"]', '2022-01-21 16:00:49', '2022-01-21 16:00:49'),
(2, 'Litmus', '1', 250, 0, '[\"Travel\"]', '2022-01-21 16:00:49', '2022-01-21 16:00:49');

-- --------------------------------------------------------

--
-- Table structure for table `stokvel_memberships`
--

CREATE TABLE `stokvel_memberships` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `stokvel_id` int(11) NOT NULL,
  `stokvel_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `membership_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project` int(11) NOT NULL,
  `transaction_amount` int(11) NOT NULL,
  `payer_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `transaction_status` int(11) NOT NULL,
  `transaction_type_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_types`
--

CREATE TABLE `transaction_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaction_type_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acc_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reg_fee` int(11) NOT NULL,
  `stokvel_admin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone_number`, `bank_name`, `acc_number`, `referral_code`, `email`, `email_verified_at`, `password`, `photo`, `gender`, `active`, `deleted_at`, `remember_token`, `reg_fee`, `stokvel_admin`, `created_at`, `updated_at`) VALUES
(1, 'Sam Makina', '0855744323', 'FNB', '62819246674', NULL, 'muzvondalewin@gmail.com', '2022-01-23 10:25:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', NULL, 'male', 1, NULL, '2JIEZictbvwi0ngl6bOhbBcv0DKjvl8CFVoZOQZwDe6oOVqLdMcc1CyPUShm', 1, '1', NULL, '2022-01-23 12:49:57'),
(2, 'Xolani Dhlamini', '0529667536', 'FNB', '62899382773', NULL, 'wadziehavazvidi@gmail.com', NULL, '$2y$10$uKZLG1uRKC7yV4pWQ1JnJeoU7vo0JYhwfa5xpdAOxcuDvc5W/k7iC', NULL, NULL, 1, NULL, NULL, 1, NULL, '2022-01-23 10:29:30', '2022-01-23 10:29:55'),
(3, 'Lindiwe Zulu', '0762736645', 'FNB', '62982378427', NULL, 'lewinitservices@gmail.com', NULL, '$2y$10$VYp4EMsRCAiNAeXAlhfAC.aRVCKJ3G8bduQxn8UrZk75i3JRbeg8.', NULL, NULL, 1, NULL, NULL, 1, NULL, '2022-01-23 18:49:55', '2022-01-23 18:50:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_structures`
--
ALTER TABLE `project_structures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_types`
--
ALTER TABLE `project_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queues`
--
ALTER TABLE `queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `referrals_user_id_unique` (`user_id`),
  ADD UNIQUE KEY `referrals_code_unique` (`code`);

--
-- Indexes for table `stokvels`
--
ALTER TABLE `stokvels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stokvel_memberships`
--
ALTER TABLE `stokvel_memberships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_types`
--
ALTER TABLE `transaction_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_phone_number_unique` (`phone_number`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `project_structures`
--
ALTER TABLE `project_structures`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project_types`
--
ALTER TABLE `project_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `queues`
--
ALTER TABLE `queues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stokvels`
--
ALTER TABLE `stokvels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stokvel_memberships`
--
ALTER TABLE `stokvel_memberships`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `transaction_types`
--
ALTER TABLE `transaction_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
