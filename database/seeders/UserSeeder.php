<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Default credentials
        \App\Models\User::insert([
            [ 
                'name' => 'Lewin',
                'email' => 'muzvondalewin@gmail.com',
                'phone_number' => '0855744323',
                'bank_name' => 'FNB',
                'acc_number' => '62819246674',
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'gender' => 'male',
                'active' => 1,
                'remember_token' => Str::random(10),
                'reg_fee' => 0,
                'stokvel_admin' => "1"
            ]
        ]);

    }
}
