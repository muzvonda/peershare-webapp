<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStokvelMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stokvel_memberships', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('stokvel_id');
            $table->string('stokvel_name');
            $table->string('member_type');
            $table->integer('membership_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stokvel_memberships');
    }
}
