<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStokvelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stokvels', function (Blueprint $table) {
            $table->id();
            $table->string('stokvel_name');
            $table->string('admins');
            $table->integer('population_limit');
            $table->integer('population');
            $table->string('project_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stokvels');
    }
}
