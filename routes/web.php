<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PeerDashboardController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\StokvelController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\FileUploadController;
use App\Http\Controllers\TransactionsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('loggedin')->group(function() {
    Route::get('login', [AuthController::class, 'loginView'])->name('login-view');
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::get('register', [AuthController::class, 'registerView'])->name('register-view');
    Route::post('register', [AuthController::class, 'register'])->name('register');
});

Route::middleware('auth')->group(function() {
    Route::get('/', [PeerDashboardController::class, 'index'])->name('dashboard');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('page/{layout}/{theme}/{pageName}', [PageController::class, 'loadPage'])->name('page');
    Route::get('home', [PeerDashboardController::class, 'index'])->name('home');
    Route::get('registration-fee', [PeerDashboardController::class, 'registrationFee'])->name('registrationfee');

    Route::get('projects', [ProjectController::class, 'index'])->name('projects');
    Route::get('create-project', [ProjectController::class, 'createProject'])->name('createproject');
    Route::post('create-project', [ProjectController::class, 'saveProject'])->name('saveproject');
    Route::get('view-project/{project_id}', [ProjectController::class, 'viewProject'])->name('viewproject');

    Route::get('stokvels', [StokvelController::class, 'stokvels'])->name('stokvels');
    Route::get('community-stokvels', [StokvelController::class, 'communityStokvels'])->name('communitystokvels');
    Route::get('create-stokvel', [StokvelController::class, 'createStokvel'])->name('createstokvel');
    Route::post('create-stokvel', [StokvelController::class, 'saveStokvel'])->name('savestokvel');
    Route::post('community-stokvels', [StokvelController::class, 'joinStokvel'])->name('joinstokvel');

    Route::get('referrals', [PeerDashboardController::class, 'referrals'])->name('referrals');
    Route::get('profile', [PeerDashboardController::class, 'profile'])->name('profile');

    Route::post('pay', [PaymentController::class, 'redirectToGateway'])->name('pay');
    Route::get('payment/callback', [PaymentController::class, 'handleGatewayCallback']);

    Route::post('approve', [TransactionsController::class, 'approveTransaction'])->name('approve');
    Route::post('recommit', [TransactionsController::class, 'approveRecommitTransaction'])->name('recommit');

    Route::get('file-upload',[FileUploadController::class, 'fileUpload'] )->name('file.upload');
    Route::post('file-upload',[FileUploadController::class, 'fileUploadPost'] )->name('file.upload.post');
});

